# JPG automatisé du menu du restaurant crous de côte de nacre

## Description
The goal is to recover the menu from this website **`"https://www.crous-normandie.fr/restaurant/restaurant-cote-de-nacre-2/"`**, then insert the informations in a html file and finally generate a jpg file from the html.
Here is an example:
![Example 24/03/2023](./output/menu.jpg)

## Roadmap
Here is the current roadmap (it may change)
1) **Automation** : generate automatically 1 img per day and post it on discord
2) **Activity** : display if the Restaurant is not open today
3) **Better graphics** : improve the front-end
4) 

## Installation
You need to install 2 libs :
- BeautifulSoup to navigate through html tags
- ImgKit to create a png file from a html file

## Usage
Simply launch the `Main.py` file with python3

## Authors and acknowledgment
Antoine BURNEL

## License
MIT-license (see the LICENCE.txt file at root)

## Project status
Still in progress