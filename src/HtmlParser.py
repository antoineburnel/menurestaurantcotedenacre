from MenuData import Menu
from bs4 import BeautifulSoup

PLAT_DU_JOUR=0
POISSON=1
GRILLADE=2
SALADERIE=3
PATES=4
PIZZA=5

def parseMenu(html:str):
    menu = Menu()
    soupDocument = BeautifulSoup(html, features="html.parser")
    updateMenu(soupDocument, menu)
    return menu

def updateMenu(soupDocument:BeautifulSoup, menu:Menu):
    soupMenu = goToMenu(soupDocument)
    menu.platDuJour = getPlat(soupMenu, PLAT_DU_JOUR)
    menu.poisson = getPlat(soupMenu, POISSON)
    menu.grillade = getPlat(soupMenu, GRILLADE)
    menu.saladerie = getPlat(soupMenu, SALADERIE)
    menu.pates = getPlat(soupMenu, PATES)
    menu.pizza = getPlat(soupMenu, PIZZA)

def getPlat(soup:BeautifulSoup, index:int):
    items = []
    soup = goNexts(soup, index)
    soup = goToChild(soup, "ul")
    for children in getChildrens(soup):
        items.append(children.text)
    if items[0] == "menu non communiqué":
        return None
    return items

def goToMenu(soup:BeautifulSoup):
    soup = goToChild(soup, "article")
    soup = goToChild(soup, "section")
    soup = goToSibling(soup, "section")
    soup = goToChild(soup, "ul")
    soup = goToChild(soup, "li")
    return soup

def goToChild(soup:BeautifulSoup, tag:str):
    return soup.findChild(tag)

def goToSibling(soup:BeautifulSoup, tag:str):
    return soup.find_next(tag)

def goNext(soup:BeautifulSoup):
    return soup.nextSibling

def goNexts(soup:BeautifulSoup, n:int):
    for _ in range(n):
        soup = goNext(soup)
    return soup

def goBack(soup:BeautifulSoup):
    return soup.parent

def getChildrens(soup:BeautifulSoup):
    return soup.children
    return items