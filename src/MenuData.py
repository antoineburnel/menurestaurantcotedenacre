def standToString(stand:str):
    if (stand == None):
        return "\t-Rien\n"
    string = ""
    for i in range(len(stand)):
        string += "\t-" + stand[i] + "\n"
    return string

class Menu():

    def __init__(self):
        self.platDuJour:str = []
        self.poisson:str = []
        self.grillade:str = []
        self.saladerie:str = []
        self.pates:str = []
        self.pizza:str = []

    def __str__(self):
        string = "Menu:\n"
        string += "Plat du jour:\n"
        string += standToString(self.platDuJour)
        string += "Poisson:\n"
        string += standToString(self.poisson)
        string += "Grillade:\n"
        string += standToString(self.grillade)
        string += "Saladerie:\n"
        string += standToString(self.saladerie)
        string += "Pates:\n"
        string += standToString(self.pates)
        string += "Pizza:\n"
        string += standToString(self.pizza)
        return string