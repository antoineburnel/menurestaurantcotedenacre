import urllib.request

def getHtmlFromUrl(url):
    fp = urllib.request.urlopen(url)
    mybytes = fp.read()
    mystr:str = mybytes.decode("utf8")
    fp.close()
    return mystr