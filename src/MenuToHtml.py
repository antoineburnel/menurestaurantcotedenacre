from MenuData import Menu
from Datas import *
import os

path = "../output/index.html"

def generateHtmlStand(nomClasse:str, nomStand:str, items:str):

    etat = ""
    if items == None:
        etat = "closed"
    else:
        etat = "opened"

    str = "\r\t\t\t<div class=\"item " + nomClasse + " " + etat + "\">\n"
    str += "\t\t\t\t<h2>" + nomStand + "</h2>\n"
    if items != None:
        for _ in range(len(items)):
            str += "\t\t\t\t<p>" + items[_] + "</p>\n"
    str += "\t\t\t</div>\n"
    return str

def generateHtmlFromMenu(menu:Menu):

    if os.path.exists(path):
        os.remove(path)
    file = open(path, "w")

    html =   """<!DOCTYPE html>
<html>
\t<head>
\t\t<title>Menu</title>
\t\t<link rel="stylesheet" href="styles.css">
\t</head>
\t<body>
\t\t<div class="crous">\n"""
    html +="\t\t\t<h1>Restaurant universitaire " + nomRestaurant + "</h1>\n"
    html += """\t\t</div>
\t\t<div class="container">"""

    html += generateHtmlStand("Poisson", "Poisson", menu.poisson)
    html += generateHtmlStand("Platdujour", "Plat du jour", menu.platDuJour)
    html += generateHtmlStand("Grillade", "Grillade", menu.grillade)
    html += generateHtmlStand("Saladerie", "Saladerie", menu.saladerie)
    html += generateHtmlStand("Pates", "Pâtes", menu.pates)
    html += generateHtmlStand("Pizza", "Pizza", menu.pizza)

    html += """\t\t</div>
\t</body>
</html>"""
    file.write(html)
    file.close()
    
