import imgkit
from MenuToHtml import generateHtmlFromMenu
from MenuData import Menu

def generateImageFromMenu(menu:Menu):
    generateHtmlFromMenu(menu)
    generateImage()
    
def generateImage():
    options = {
        'format': 'jpg',
        'width': 1920,
        'height': 1080
    }
    imgkit.from_file('../output/index.html',
        options=options,
        css='../template/styles.css',
        output_path='../output/menu.jpg'
    )