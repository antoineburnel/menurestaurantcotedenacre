from Datas import *
from MenuData import Menu
from HtmlRequest import getHtmlFromUrl
from HtmlParser import parseMenu
from MenuToPng import generateImageFromMenu

if __name__ == "__main__":
    htmlCode:str = getHtmlFromUrl(urlRestaurant)
    menu:Menu = parseMenu(htmlCode)
    generateImageFromMenu(menu)